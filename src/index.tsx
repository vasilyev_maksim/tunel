import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';

import './index.scss';
import './slider.less';

ReactDOM.render(
    <App />,
    document.getElementById('app'),
);
