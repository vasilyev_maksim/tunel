import * as React from 'react';
import { range } from 'lodash';

interface IProps {
    itemsCount: number;
    vratio: number;
    hratio: number;
}

export const Frame: React.SFC<IProps> = ({ itemsCount, vratio, hratio }) => {
    return (
        <div className="frame">
            {range(0, itemsCount + 1).map(i => {
                const size = 100 * (i) / itemsCount + '%';
                return (
                    <div
                        key={i}
                        className="item"
                        style={{
                            zIndex: -i,
                            height: size,
                            width: size,
                            transform: `
                            translateY(${vratio * (itemsCount - i) / itemsCount}%)
                            translateX(${hratio * (itemsCount - i) / itemsCount}%)
                            `,
                            animationDelay: 50 * i + 'ms',
                        }}
                    />
                );
            })}
        </div>
    );
};
