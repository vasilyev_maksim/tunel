import * as React from 'react';
import { DeviceOrientation } from 'react-fns';

export const Frame: React.SFC = () => {
    return (
        <DeviceOrientation
            render={({ alpha, beta, gamma, absolute }) => (
                <pre>{JSON.stringify({ alpha, beta, gamma, absolute }, null, 2)}</pre>
            )}
        />
    );
};