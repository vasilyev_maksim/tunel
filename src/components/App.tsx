import * as React from 'react';
import { Frame } from './Frame';
import Slider from 'rc-slider';

interface IState {
    itemsCount: number;
    vratio: number;
    hratio: number;
}

class App extends React.Component<{}, IState> {
    public state: IState = {
        itemsCount: 18,
        vratio: 0,
        hratio: 0,
    };

    render () {
        return (
            <>
                <div className="slider">
                    Items count: {this.state.itemsCount}
                    <Slider
                        min={1}
                        max={30}
                        step={1}
                        value={this.state.itemsCount}
                        onChange={(itemsCount) => this.setState({ itemsCount })}
                    />

                    <br />

                    V ratio: {this.state.vratio}
                    <Slider
                        min={-50}
                        max={50}
                        value={this.state.vratio}
                        onChange={(vratio) => this.setState({ vratio })}
                    />

                    H ratio: {this.state.hratio}
                    <Slider
                        min={-50}
                        max={50}
                        value={this.state.hratio}
                        onChange={(hratio) => this.setState({ hratio })}
                    />
                </div>

                <Frame
                    itemsCount={this.state.itemsCount}
                    vratio={this.state.vratio}
                    hratio={this.state.hratio}
                />
            </>
        );
    }
}

export default App;